open Structures

(* ------------------------------------------------------------------------- *)
(* Manipulating generative, empirical & finitely supported functions. *)

type 'a gen
(** Generative probability (i.e. sampler) *)

type 'a emp
(** Empirical probability *)

type 'a fin_fun
(** Finitely supported function *)

type 'a fin_den = private 'a fin_fun
(** Finitely supported density *)

type 'a fin_prb = private 'a fin_fun
(** Finitely supported probability *)

val compare_prb : 'a fin_prb -> 'a fin_prb -> int
(** Total order on finitely supported probabilities, inherited from
    underlying total order. *)

val sample_gen : 'a gen -> 'a
(** Sample from generative probability *)

val sample_emp : 'a emp -> 'a
(** Sample from empirical probability *)

val sample_prb : 'a fin_prb -> 'a
(** Sample from finitely supported probability *)

val map_gen : ('a -> 'b) -> 'a gen -> 'b gen
(** Functorial action on generative probabilities *)

val map_emp : ('a -> 'b) -> 'a emp -> 'b emp
(** Functorial action on empirical probabilities *)

val generative : sampler: (unit -> 'a) -> 'a gen
(** Creates a generative probability distribution from a sampler. *)

val density : (module Ordered with type t = 'elt) -> ('elt * float) list -> 'elt fin_den
(** Creates a finitely supported _density_ from weighted points.
    A density is not necessarily normalized.
    The underlying set needs to be totally ordered. *)

val normalize : 'a fin_den -> 'a fin_prb
(** Normalize a density to obtain a probability measure.
    This amounts to integrate the density wrt the uniform measure
    to obtain (via Riesz) a probability. *)

val empirical_of_generative : nsamples: int -> 'a gen -> 'a emp
(** Samples (under iid assumptions) [nsamples] times from [sampler], producing
    an empirical distribution. *)

val empirical_of_raw_data : 'a array -> 'a emp
(** Returns the empirical probability distribution associated to a raw array of samples. *)

val fin_prb_of_empirical : (module Ordered with type t = 'elt) -> 'elt emp -> 'elt fin_prb
(** Computes density from empirical distribution. *)

val uniform : (module Ordered with type t = 'elt) -> 'elt array -> 'elt fin_prb
(** Finitely supported uniform distribution. *)

val eval_prb : 'elt fin_prb -> 'elt -> float
(** Evaluates finitely supported probability on argument. Returns 0 if
    the argument is out of the support. *)

val empirical_float_random : nsamples: int -> samplers: float gen array -> float emp array
(** Samples (under iid assumptions) [nsamples] times from each element
    of [samplers], producing an array of empirical distributions.
    The sequence of calls to each sampler is random, so as to avoid
    background noise. *)

val truncate : (module Ordered with type t = 'elt) -> 'elt emp -> float -> 'elt emp
(** [truncate (module O) dist p] conditions an empirical distribution
    on a totally ordered space on the lower subset containing [p] mass. *)

val quantile : (module Ordered with type t = 'elt) -> 'elt emp -> float -> 'elt
(** [quantile (module O) dist p] returns the value x that splits the support of
    the distribution into a lower subset of mass [p] and an upper subset of
    mass [1-p]. *)

val mean : (module Linear with type t = 'elt) -> 'elt emp -> 'elt
(** Computes the mean of a distribution that lives on a linear space. *)

val variance : float emp -> float
(** Computes the variance of a distribution on floats. Note that we use the
    biased estimator for empirical distributions, so care should be taken
    for small samples sizes (e.g. #samples > 50 is good). *)

val remove_outliers : nsigmas:float -> float emp -> float emp
(** Remove data points which are [nsigmas] standard deviations above (or below)
    the mean. *)

val raw_data_empirical : 'elt emp -> [`Empirical of 'elt array]
(** Returns the raw data underlying an empirical distribution. *)

val raw_data_density : 'elt fin_den -> [`Density of ('elt * float) list]
(** Returns the raw data underlying a finitely supported density. *)

val raw_data_probability : 'elt fin_prb -> [`Probability of ('elt * float) list]
(** Returns the raw data underlying a finitely supported probability. *)

val pp_fin_fun : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a fin_fun -> unit
(** Pretty print density or finitely supported probability. *)

val proj_ordered : 'a fin_fun -> (module Ordered with type t = 'a)
(** Projects out the ordered structure underlying the support of the finitely
    supported function. *)

val coin : bias:float -> bool fin_prb
(** Biased coin. Raises an error if [bias] is not in [0,1]. *)

val exponential : rate:float -> float gen
(** Exponential distribution via inverse CDF. *)

val gaussian : mean:float -> std:float -> float gen
(** Gaussian distribution via Box-Muller transform.
    Returns a pair of _independent_ gaussian variates with
    prescribed mean and standard deviation. *)

val binomial : bool fin_prb -> int -> int fin_prb
(** Binomial distribution.
    [binomial p n] returns the probability of having
    [k] successes over [n] experiments, according to
    a biased coin [p]. *)
