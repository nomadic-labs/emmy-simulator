(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open StaTz

(* ------------------------------------------------------------------------- *)
(* Auxiliary functions/modules *)

module Int_map = Map.Make (Structures.Int)

let memoize_int : (int -> 'a) -> int -> 'a =
 fun f ->
  let t = ref Int_map.empty in
  fun i ->
    match Int_map.find_opt i !t with
    | None ->
        let res = f i in
        let map = Int_map.add i res !t in
        t := map ;
        res
    | Some res ->
        res

(* ------------------------------------------------------------------------- *)
(* Simulator definition *)

module type Simulation_params_sig = sig
  val max_delay_before_chain_death : float (* in seconds *)

  val bakers_priority_list_length : int

  val debug_log : bool
end

module Make
    (Proto : Protocol.S)
    (P : Player.S)
    (Helpers : Player.Helpers_S with type Player_map.key = P.t)
    (Params : Simulation_params_sig) =
struct
  open Helpers

  let debug : ('a, Format.formatter, unit) format -> 'a =
   fun fmt ->
    if Params.debug_log then Format.eprintf fmt
    else Format.ifprintf Format.err_formatter fmt

  (* ----------------------------------------------------------------------- *)
  (* Instantiate baking game *)

  module Game = Baking.Make (Proto) (P) (Helpers)

  type chain = Game.block list

  (* ----------------------------------------------------------------------- *)
  (* User-friendliness *)

  let chain_encoding : chain Data_encoding.t =
    Data_encoding.list Game.block_encoding

  let pp_chain fmtr chain = Format.pp_print_list Game.pp_block fmtr chain

  (* ----------------------------------------------------------------------- *)

  let players_map : unit Player_map.t =
    Array.fold_left
      (fun acc player -> Player_map.add player () acc)
      Player_map.empty
      P.all

  (* ----------------------------------------------------------------------- *)
  (* Definition of the basic samplers *)

  (* Finitely supported probability on players, proportional to their share of
     the stake *)
  let players_fin_prob : P.t Stats.fin_prb =
    let weights =
      Array.map (fun player -> (player, P.share player)) P.all |> Array.to_list
    in
    Stats.(normalize (density (module P) weights))

  (* Finitely supported prob. on _indices_ of players *)
  let players_index_fin_prob : int Stats.fin_prb =
    let weights =
      Array.mapi (fun index player -> (index, P.share player)) P.all
      |> Array.to_list
    in
    Stats.(normalize (density (module Structures.Int) weights))

  (* Sampling players *)
  let player_gen : P.t M.t =
    Stats.generative ~sampler:(fun () -> Stats.sample_prb players_fin_prob)

  (* Sampling player indices *)
  let player_index_gen : int M.t =
    Stats.generative ~sampler:(fun () ->
        Stats.sample_prb players_index_fin_prob)

  (* folds from 0 to n-1 included *)
  let fold_i n f init =
    let rec loop i acc = if i >= n then acc else loop (i + 1) (f i acc) in
    loop 0 init

  (* Sampling [Game.level] *)
  let level_gen : Game.level M.t =
    Stats.generative ~sampler:(fun () ->
        let block_priority =
          (* TODO: make this simpler *)
          let priority = Array.map (fun _ -> max_int) P.all in
          fold_i
            Params.bakers_priority_list_length
            (fun prio () ->
              let player_index = M.run player_index_gen in
              priority.(player_index) <- min priority.(player_index) prio)
            () ;
          fold_i
            (Array.length priority)
            (fun player_index acc ->
              Int_vec.(
                add acc (basis P.all.(player_index) priority.(player_index))))
            Int_vec.zero
        in
        let endorsers =
          (* TODO: This should just be a multinomial sample.
             Here, we sample endorsement slots in the most naive way possible. *)
          let endo_counts = Array.map (fun _ -> 0) P.all in
          fold_i
            Proto.endorsers_per_block
            (fun _slot () ->
              let player_index = M.run player_index_gen in
              let p = Random.float 1.0 in
              if p > P.health P.all.(player_index) then ()
              else endo_counts.(player_index) <- endo_counts.(player_index) + 1)
            () ;
          fold_i
            (Array.length endo_counts)
            (fun player_index acc ->
              Int_vec.(
                add acc (basis P.all.(player_index) endo_counts.(player_index))))
            Int_vec.zero
        in
        Game.{block_priority; endorsers})

  (* Sampling [Game.levels] *)
  let levels_gen : Game.levels M.t =
    Stats.generative ~sampler:(fun () ->
        memoize_int (fun _height ->
            let level = Stats.sample_gen level_gen in
            debug "At height %d, level = %a@." _height Game.pp_level level ;
            level))

  (* ----------------------------------------------------------------------- *)

  let fresh_uid =
    let x = ref 0 in
    fun () ->
      let v = !x in
      incr x ; v

  let genesis_block : Game.block =
    {
      age = 0.0;
      fitness = 0;
      rewards = Float_vec.zero;
      level = Game.genesis_level;
      outcome = Game.genesis_outcome;
      block_uid = Block_id.fresh ();
    }

  let genesis_chain = [genesis_block]

  let make_head : Game.outcome -> Game.level -> Game.block -> Game.block =
   fun outcome level prev ->
    let rewards =
      Float_vec.(
        add prev.rewards (basis outcome.Game.baker outcome.Game.block_reward))
    in
    let rewards = Float_vec.add outcome.Game.endorsement_rewards rewards in
    {
      age = prev.age +. outcome.Game.delay;
      fitness = prev.fitness + 1;
      rewards;
      level;
      outcome;
      block_uid = Block_id.fresh ();
    }

  (* Conveniently accessing chain heads *)
  let chain_head_exn : chain -> Game.block = function
    | [] ->
        (* empty chain *)
        assert false
    | head :: _ ->
        head

  let height (chains : chain list) =
    match chains with
    | [] | [] :: _ ->
        assert false
    | ({fitness; _} :: _) :: _ ->
        fitness

  let age : chain -> float =
   fun chain ->
    match chain with [] -> assert false | block :: _ -> block.Game.age

  (* ----------------------------------------------------------------------- *)

  (* Simulation step *)
  let step :
      Game.arena -> Game.levels -> chain list -> (chain list * Game.arena) M.t
      =
   fun arena levels chains ->
    let open M in
    let height = height chains in
    let height = height + 1 in
    let level = levels height in
    debug "level: %a@." Game.pp_level level ;
    let chains =
      List.map
        (function [] -> assert false | block :: chain -> (block, chain))
        chains
    in
    let heads = List.map (fun (block, _chain) -> block) chains in
    Game.bake height levels heads arena
    >>= fun (outcomes, arena) ->
    let () = assert (outcomes <> []) in
    let outcomes =
      List.mapi
        (fun i outcome ->
          let (prev_block, chain) =
            try
              List.find
                (fun ({Game.block_uid; _}, _chain) ->
                  Block_id.equal block_uid outcome.Game.prev_uid)
                chains
            with Not_found -> assert false
          in
          let new_head = make_head outcome level prev_block in
          debug "head %d: %a@." i Game.pp_block new_head ;
          new_head :: prev_block :: chain)
        outcomes
    in
    return (outcomes, arena)

  (* Sample blocks until a predicate on chains is true *)
  let rec iter_until arena predicate levels chains =
    let open M in
    if predicate chains then return chains
    else (
      debug "---------------- ~~~ ----------------@." ;
      debug "%d chains alive@." (List.length chains) ;
      step arena levels chains
      >>= fun (chains, arena) -> iter_until arena predicate levels chains )

  (* At the end of the simulation, we pick the youngest chain as the winning one.
     In case of tie, we pick an arbitrary one (implem. dep.). *)
  let youngest (chains : chain list) =
    let chains =
      List.sort
        (fun c1 c2 ->
          if age c1 < age c2 then -1 else if age c1 > age c2 then 1 else 0)
        chains
    in
    debug "at end of simulation: %d chains left@." (List.length chains) ;
    List.iter
      (fun chain ->
        debug "%a@." pp_chain chain ;
        debug "><><><><><><><><><><")
      chains ;
    match chains with [] -> assert false | youngest :: _ -> youngest

  (* Useful predicates on chains *)

  (* True whenever we have a chain of length n *)
  let height_predicate n : chain list -> bool =
   fun chains ->
    List.for_all
      (* List.exists should be equivalent here, chains should have the same length *)
        (fun chain ->
        let {Game.fitness; _} = chain_head_exn chain in
        fitness >= n)
      chains

  (* True whenever the youngest chain is older than [t] seconds *)
  let age_predicate t : chain list -> bool =
   fun chains ->
    List.for_all
      (fun chain ->
        let {Game.age; _} = chain_head_exn chain in
        age >= t)
      chains

  let simulate_chains arena predicate =
    let open M in
    levels_gen
    >>= fun levels -> iter_until arena predicate levels [genesis_chain]

  (* In case there are several chains at the end of the chain sampling process,
     we pick the youngest one *)
  let simulate arena predicate =
    M.map youngest (simulate_chains arena predicate)
end
